package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Richard" ) );
	}
	
	@Test
	public void testIsValidLoginBoundary( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "derek" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "d" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "derek123" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "derek1" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1derek" ) );
	}
	
	@Test
	public void testIsValidLoginNumbersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "123456" ) );
	}

}
